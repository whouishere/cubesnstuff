# Cubes n' Stuff

Cubes n' Stuff is a Minecraft clone made with a custom C++ OpenGL engine.

This project is solely made for educational purposes, mainly for me to practice OOP, meta programming and fairly new C++. It isn't even playable _yet_.

## TODO
- Initiate/Add entities and objects with operator overloading (`<<` or `+=`?)
- Search an actual way to load and store chunks properly
- Use multithreading with `std::async` and `std::mutex`

## Structure ideas
- Store scenes' blocks on multi-dimensional lists? (probably not good idea)
- Keep track of last modified/added blocks queue on a scene, in which the renderer can look for and update it, with no need to check for the entire scene for new updates

## Build
Basic requirements are
- CMake >= 3.11
- C++17 compatible compiler
- Python 3

Then you can use
```
cmake -B build
cmake --build build
```

CMake will fetch and build the dependencies then build the game.

## License
Cubes n' Stuff is licensed under [GPL 3.0](https://www.gnu.org/licenses/gpl-3.0.html) or later. You may freely copy, distribute and modify it. Any modifications must also be distributed under GPL. You can read the [COPYING](./COPYING) file for more information.
