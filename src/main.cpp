#include <cstdlib>
#include <exception>
#include <iostream>

// renderer has include privilege
#include "engine/renderer.hpp"
#include "engine/texture.hpp"
#include "engine/window.hpp"
#include "game.hpp"

int main() {
	try {
		auto game = Game();
		while (game.IsRunning()) {
			game.Update();
		}
	} catch (std::exception const& e) {
		std::cerr << "FATAL ERROR: " << e.what() << "\n";
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
