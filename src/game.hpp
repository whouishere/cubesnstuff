#ifndef GAME_HPP
#define GAME_HPP

#include <memory>
#include <vector>

#include "engine/renderer.hpp"
#include "engine/sprite.hpp"
#include "engine/texture.hpp"
#include "engine/window.hpp"

#define GAME_WINDOW_WIDTH  640
#define GAME_WINDOW_HEIGHT 480
#define GAME_WINDOW_TITLE  "Cubes n' Stuff"

class Game {
public:
	Game();

	bool IsRunning();
	void Update();

private:
	Engine::Window m_window;
	Engine::Renderer m_renderer;
	std::vector<std::unique_ptr<Engine::Texture>> m_textures;

	bool m_quit;
	Engine::Sprite m_sprite1;
	Engine::Sprite m_sprite2;
	Engine::Sprite m_plyr;

	void UpdateInput();
};

#endif // GAME_HPP
