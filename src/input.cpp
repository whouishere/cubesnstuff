#include "input.hpp"

#include <GLFW/glfw3.h>

bool Input::IsKeyPressed(GLFWwindow* window, const Key key) {
	return glfwGetKey(window, key) == GLFW_PRESS;
}
