#ifndef INPUT_HPP
#define INPUT_HPP

#include <GLFW/glfw3.h>

enum Key {
	ESCAPE = GLFW_KEY_ESCAPE,
	SPACE = GLFW_KEY_SPACE,
	NUM_0 = GLFW_KEY_0,
	NUM_1 = GLFW_KEY_1,
	NUM_2 = GLFW_KEY_2,
	NUM_3 = GLFW_KEY_3,
	NUM_4 = GLFW_KEY_4,
	NUM_5 = GLFW_KEY_5,
	NUM_6 = GLFW_KEY_6,
	NUM_7 = GLFW_KEY_7,
	NUM_8 = GLFW_KEY_8,
	NUM_9 = GLFW_KEY_9,
	LEFT = GLFW_KEY_LEFT,
	RIGHT = GLFW_KEY_RIGHT,
	DOWN = GLFW_KEY_DOWN,
	UP = GLFW_KEY_UP
};

namespace Input {
	bool IsKeyPressed(GLFWwindow* window, const Key key);
};

#endif // INPUT_HPP
