#include "game.hpp"

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include <glm/ext/matrix_float4x4.hpp>
#include <glm/ext/vector_float3.hpp>
#include <glm/gtc/matrix_transform.hpp>

// renderer has include privilege
#include "engine/renderer.hpp"
#include "engine/sprite.hpp"
#include "engine/window.hpp"
#include "input.hpp"

static const std::string textures_path[] = {
	"res/container.jpg", 
	"res/wall.jpg"
};

Game::Game()
: m_window(GAME_WINDOW_WIDTH, GAME_WINDOW_HEIGHT, GAME_WINDOW_TITLE), 
  m_renderer(), 
  m_textures(), 
  m_quit(false), 
  m_sprite1(textures_path[0], glm::vec2(-0.5f, -0.5f)),
  m_sprite2(textures_path[1], glm::vec2(0.5f, 0.5f)),
  m_plyr(textures_path[0], glm::vec2(0.0f, 0.0f))
{
	m_plyr.scale = 0.5f;
}

bool Game::IsRunning() {
	return !m_quit;
}

void Game::Update() {
	m_renderer.Clear();
	m_quit = m_window.getWindowShouldClose();

	UpdateInput();

	m_sprite1.scale = glm::sin(m_window.getTime());
	m_sprite2.rotation = m_window.getTime();

	m_renderer.Draw(&m_sprite1);
	m_renderer.Draw(&m_sprite2);
	m_renderer.Draw(&m_plyr);
	m_window.Update();
}

void Game::UpdateInput() {
	const auto ctx = m_window.getWindowContext();

	if (Input::IsKeyPressed(ctx, Key::ESCAPE)) {
		m_window.setWindowShouldClose(true);
	}

	const float plyr_velocity = 0.05f;

	if (Input::IsKeyPressed(ctx, Key::LEFT)) {
		m_plyr.pos.x -= plyr_velocity;
	}
	if (Input::IsKeyPressed(ctx, Key::RIGHT)) {
		m_plyr.pos.x += plyr_velocity;
	}
	if (Input::IsKeyPressed(ctx, Key::UP)) {
		m_plyr.pos.y += plyr_velocity;
	}
	if (Input::IsKeyPressed(ctx, Key::DOWN)) {
		m_plyr.pos.y -= plyr_velocity;
	}
}
