// glad must be included before anything else
#include <glad/gl.h>

#include "window.hpp"

#include <cstdint>
#include <iostream>
#include <stdexcept>
#include <string>

#include <GLFW/glfw3.h>

Engine::Window::Window(std::uint32_t width, std::uint32_t height, const std::string &title) {
	glfwSetErrorCallback([](int error, const char* description) {
		std::cerr << "GLFW Error " << error << ": " << description << "\n";
	});

	if (!glfwInit()) {
		throw std::runtime_error("GLFW was unable to initialize");
	}

	std::cout << "GLFW " << GLFW_VERSION_MAJOR << "." << GLFW_VERSION_MINOR << "\n";

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	m_context = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);
	if (!m_context) {
		glfwTerminate();
		throw std::runtime_error("Unable to create window");
	}

	glfwMakeContextCurrent(m_context);
	gladLoadGL(glfwGetProcAddress);
	glViewport(0, 0, width, height);

	glfwSwapInterval(1);
	glfwSetFramebufferSizeCallback(m_context, [](GLFWwindow* _, int w, int h){
		glViewport(0, 0, w, h);
	});
}

Engine::Window::~Window() {
	glfwDestroyWindow(m_context);
	glfwTerminate();
}

void Engine::Window::Update() {
	glfwSwapBuffers(m_context);
	glfwPollEvents();
}

GLFWwindow* Engine::Window::getWindowContext() const {
	return m_context;
}

float Engine::Window::getTime() const {
	return static_cast<float>(glfwGetTime());
}

bool Engine::Window::getWindowShouldClose() const {
	return glfwWindowShouldClose(m_context);
}

void Engine::Window::setWindowShouldClose(bool close) {
	glfwSetWindowShouldClose(m_context, close);
}
