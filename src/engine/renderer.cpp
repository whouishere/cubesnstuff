#include "renderer.hpp"

#include <glad/gl.h>

#include "util.hpp"
#include "shader.hpp"

#define DEFAULT_VERTEX_SHADER "res/textured.vs"
#define DEFAULT_FRAGMENT_SHADER "res/textured.fs"
#define DEFAULT_CLEAR_COLOR 0.3f, 0.3f, 0.3f, 1.0f

const static GLfloat vertices_data[] = {
	0.25f,  0.25f, 0.0f, 1.0f, 0.0f, 0.0f,	1.0f, 1.0f, // top right
	0.25f, -0.25f, 0.0f, 0.0f, 1.0f, 0.0f,	1.0f, 0.0f, // bottom right
  -0.25f, -0.25f, 0.0f, 0.0f, 0.0f, 1.0f,   0.0f, 0.0f, // bottom left
  -0.25f,  0.25f, 0.0f, 1.0f, 1.0f, 0.0f,	0.0f, 1.0f  // top left
};

const static GLint indices_data[] = {
	0, 1, 3, 
	1, 2, 3
};

Engine::Renderer::Renderer()
: shader(DEFAULT_VERTEX_SHADER, DEFAULT_FRAGMENT_SHADER)
{
	GL_CHECK(glGenBuffers(1, &m_VBO));
	GL_CHECK(glGenBuffers(1, &m_EBO));
	GL_CHECK(glGenVertexArrays(1, &m_VAO));
	GL_CHECK(glBindVertexArray(m_VAO));

	GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, m_VBO));
	GL_CHECK(glBufferData(GL_ARRAY_BUFFER, sizeof(vertices_data), vertices_data, GL_STATIC_DRAW));

	GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO));
	GL_CHECK(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices_data), indices_data, GL_STATIC_DRAW));

	GL_CHECK(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (void*) 0));
	GL_CHECK(glEnableVertexAttribArray(0));

	GL_CHECK(glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (void*) (3 * sizeof(GLfloat))));
	GL_CHECK(glEnableVertexAttribArray(1));

	GL_CHECK(glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (void*) (6 * sizeof(GLfloat))));
	GL_CHECK(glEnableVertexAttribArray(2));

	GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, 0));
}

Engine::Renderer::~Renderer() {
	GL_CHECK(glDeleteBuffers(1, &m_VBO));
	GL_CHECK(glDeleteVertexArrays(1, &m_VAO));
}

void Engine::Renderer::Clear() {
	GL_CHECK(glClearColor(DEFAULT_CLEAR_COLOR));
	GL_CHECK(glClear(GL_COLOR_BUFFER_BIT));
}

void Engine::Renderer::Draw() {
	shader.Use();
	DrawElements();
}

void Engine::Renderer::Draw(Engine::Sprite *sprite) {
	sprite->SetupDraw(&shader);
	DrawElements();
}

void Engine::Renderer::DrawElements() {
	GL_CHECK(glBindVertexArray(m_VAO));
	GL_CHECK(glDrawElements(GL_TRIANGLES, (sizeof(indices_data) / sizeof(*indices_data)), GL_UNSIGNED_INT, 0));
}
