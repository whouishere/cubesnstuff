#ifndef SHADER_HPP
#define SHADER_HPP

#include <string>

#include <glad/gl.h>
#include <glm/ext/matrix_float4x4.hpp>

namespace Engine {
	class Shader {
	public:
		Shader(const std::string &vertex_path, const std::string &fragment_path);
		~Shader();

		void Use();

		void setInt(const std::string &name, GLint value);
		void setFloat(const std::string &name, GLfloat value);
		void setMat4(const std::string &name, glm::mat4 value);

	private:
		GLuint m_ID;

		[[nodiscard]] GLuint CompileShader(std::string &code, const GLenum &type);
		[[nodiscard]] GLuint CreateProgram(const GLuint *vertex, const GLuint *fragment);
		void CheckErrors(const GLuint *target, const std::string &type);
	};
};

#endif // SHADER_HPP
