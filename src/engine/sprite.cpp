#include "sprite.hpp"

#include <memory>
#include <string>

#include <glm/ext/matrix_float4x4.hpp>
#include <glm/ext/vector_float2.hpp>
#include <glm/ext/vector_float3.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "shader.hpp"
#include "texture.hpp"

Engine::Sprite::Sprite(const std::string &texture_filename, glm::vec2 pos)
: pos(pos),
  m_texture(std::make_unique<Engine::Texture>(texture_filename))
{ }

Engine::Sprite::Sprite(std::unique_ptr<Engine::Texture> texture, glm::vec2 pos)
: pos(pos),
  m_texture(std::move(texture))
{ }

void Engine::Sprite::SetupDraw(Engine::Shader *shader) {
	shader->Use();

	m_model = glm::mat4(1.0f);
	m_model = glm::translate(m_model, glm::vec3(pos, 0.0f));
	m_model = glm::scale(m_model, glm::vec3(scale, scale, scale));

	if (rotation != 0.0f) {
		m_model = glm::rotate(m_model, rotation, glm::vec3(0.0f, 0.0f, 1.0f));
	}

	shader->setMat4("model", m_model);
	m_texture->Activate();
}
