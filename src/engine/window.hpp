#ifndef WINDOW_HPP
#define WINDOW_HPP

#include <cstdint>
#include <string>

#include <GLFW/glfw3.h>

namespace Engine {
	class Window {
	public:
		Window(std::uint32_t width, std::uint32_t height, const std::string &title);
		~Window();

		void Update();

		GLFWwindow* getWindowContext() const;

		float getTime() const;
		bool getWindowShouldClose() const;
		void setWindowShouldClose(bool close);

	private:
		GLFWwindow *m_context;
	};
};

#endif // WINDOW_HPP
