#include "util.hpp"

#include <fstream>
#include <sstream>
#include <string>

[[nodiscard]] bool Engine::Util::has_suffix(const std::string &str, const std::string &suffix) {
	return str.size() >= suffix.size() && 
	       str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0;
}

[[nodiscard]] std::string Engine::Util::read_file(const std::string &filename) {
	std::stringstream file_stream;
	std::ifstream file;

	// ensure the file can throw exceptions
	file.exceptions(std::ifstream::failbit | std::ifstream::badbit);

	try {
		file.open(filename);
		file_stream << file.rdbuf();
	} catch (std::ifstream::failure const& e) {
		throw;
	}

	return file_stream.str();
}
