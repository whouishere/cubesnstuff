#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include <cstdint>
#include <string>

#include <glad/gl.h>

namespace Engine {
	class Texture {
	public:
		Texture(const std::string &filename);
		Texture() = default;
		~Texture();

		// throws an exception if texture wasn't loaded or texture unit isn't set
		void Activate() const;
		void LoadTexture(const std::string &filename);

		// throws an exception if texture wasn't loaded
		GLuint* getTexture();

	private:
		GLuint m_texture;
		std::string m_textureName;
		bool m_textureLoaded = false;

		// throws an exception if a texture isn't loaded
		void throwCheckTexture() const;
	};
};

#endif // TEXTURE_HPP
