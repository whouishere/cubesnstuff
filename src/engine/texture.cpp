#include "texture.hpp"

#include <cstdint>
#include <stdexcept>

#define STB_IMAGE_IMPLEMENTATION
#include <glad/gl.h>
#include "stb_image.h"

#include "util.hpp"

Engine::Texture::Texture(const std::string &filename) {
	LoadTexture(filename);
}

Engine::Texture::~Texture() {
	throwCheckTexture();

	GL_CHECK(glDeleteTextures(1, &m_texture));

	m_textureLoaded = false;
}

void Engine::Texture::Activate() const {
	throwCheckTexture();

	GL_CHECK(glActiveTexture(GL_TEXTURE0));
	GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_texture));
}

void Engine::Texture::LoadTexture(const std::string &filename) {
	GL_CHECK(glGenTextures(1, &m_texture));
	GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_texture));

	GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT));
	GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT));
	GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR));
	GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));

	GLint width, height, colorChannels;
	stbi_set_flip_vertically_on_load(true);
	std::uint8_t *image = stbi_load(filename.c_str(), &width, &height, &colorChannels, 0);

	if (!image) {
		throw std::runtime_error("Failed to load texture \"" + filename + "\"");
	}

	GL_CHECK(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 
	                      width, height, 0, 
						  Util::has_suffix(filename, ".png") ? GL_RGBA : GL_RGB, 
						  GL_UNSIGNED_BYTE, image));
	GL_CHECK(glGenerateMipmap(GL_TEXTURE_2D));
	GL_CHECK(glBindTexture(GL_TEXTURE_2D, 0));

	stbi_image_free(image);

	m_textureName = filename;
	m_textureLoaded = true;
}

GLuint* Engine::Texture::getTexture() {
	throwCheckTexture();
	return &m_texture;
}

void Engine::Texture::throwCheckTexture() const {
	if (!m_textureLoaded) {
		throw std::runtime_error("Tried to access an unloaded texture (" + m_textureName + ")");
	}
}
