#include "shader.hpp"

#include <iostream>
#include <stdexcept>
#include <string>

#include <glm/ext/matrix_float4x4.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "util.hpp"

#define SHADER_LOG_BUFFER 1024

Engine::Shader::Shader(const std::string &vertex_path, const std::string &fragment_path) {
	std::string vs_code;
	std::string fs_code;

	try {
		vs_code = Util::read_file(vertex_path);
		fs_code = Util::read_file(fragment_path);
	} catch (std::exception const& e) {
		throw std::runtime_error(std::string("Couldn't read shader file: ") + e.what());
	}

	GLuint vertex = CompileShader(vs_code, GL_VERTEX_SHADER);
	GLuint fragment = CompileShader(fs_code, GL_FRAGMENT_SHADER);
	m_ID = CreateProgram(&vertex, &fragment);
}

Engine::Shader::~Shader() {
	GL_CHECK(glDeleteProgram(m_ID));
}

void Engine::Shader::Use() {
	GL_CHECK(glUseProgram(m_ID));
}

void Engine::Shader::setInt(const std::string &name, GLint value) {
	const GLint loc = GL_CHECK(glGetUniformLocation(m_ID, name.c_str()));
	GL_CHECK(glUniform1i(loc, value));
}

void Engine::Shader::setFloat(const std::string &name, GLfloat value) {
	const GLint loc = GL_CHECK(glGetUniformLocation(m_ID, name.c_str()));
	GL_CHECK(glUniform1f(loc, value));
}

void Engine::Shader::setMat4(const std::string &name, glm::mat4 value) {
	const GLint loc = GL_CHECK(glGetUniformLocation(m_ID, name.c_str()));
	GL_CHECK(glUniformMatrix4fv(loc, 1, false, glm::value_ptr(value)));
}

[[nodiscard]] GLuint Engine::Shader::CompileShader(std::string &code, const GLenum &type) {
	GLuint shader;
	const char *code_cstr = code.c_str();

	shader = GL_CHECK(glCreateShader(type));
	GL_CHECK(glShaderSource(shader, 1, &code_cstr, nullptr));
	GL_CHECK(glCompileShader(shader));

	switch (type) {
		case GL_VERTEX_SHADER:
			CheckErrors(&shader, "VERTEX");
			break;
		
		case GL_FRAGMENT_SHADER:
			CheckErrors(&shader, "FRAGMENT");
			break;
	}

	return shader;
}

[[nodiscard]] GLuint Engine::Shader::CreateProgram(const GLuint *vertex, const GLuint *fragment) {
	GLuint program = GL_CHECK(glCreateProgram());
	GL_CHECK(glAttachShader(program, *vertex));
	GL_CHECK(glAttachShader(program, *fragment));
	GL_CHECK(glLinkProgram(program));

	CheckErrors(&program, "PROGRAM");

	GL_CHECK(glDeleteShader(*vertex));
	GL_CHECK(glDeleteShader(*fragment));

	return program;
}

void Engine::Shader::CheckErrors(const GLuint *target, const std::string &type) {
	int success;
	char infoLog[SHADER_LOG_BUFFER] = {0};

	if (type == "PROGRAM") {
		GL_CHECK(glGetProgramiv(*target, GL_LINK_STATUS, &success));
		if (!success) {
			GL_CHECK(glGetProgramInfoLog(*target, SHADER_LOG_BUFFER, nullptr, infoLog));
			std::cerr << "Linking of type " << type << " failed.\n";
		}
	}
	else {
		GL_CHECK(glGetShaderiv(*target, GL_COMPILE_STATUS, &success));
		if (!success) {
			GL_CHECK(glGetShaderInfoLog(*target, SHADER_LOG_BUFFER, nullptr, infoLog));
			std::cerr << "Compiling of type " << type << " failed.\n";
		}
	}

	if (infoLog[0] != 0) {
		std::cerr << infoLog << "\n";
	}
}
