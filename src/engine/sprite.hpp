#ifndef SPRITE_HPP
#define SPRITE_HPP

#include <memory>
#include <string>

#include <glm/ext/matrix_float4x4.hpp>
#include <glm/ext/vector_float2.hpp>

#include "shader.hpp"
#include "texture.hpp"

namespace Engine {
	class Sprite {
	public:
		Sprite(const std::string &texture_filename, glm::vec2 pos);
		Sprite(std::unique_ptr<Engine::Texture> texture, glm::vec2 pos);

		// setup matrices and texture for the renderer to draw
		void SetupDraw(Engine::Shader *shader);

		glm::vec2 pos;
		float scale = 1.0f;
		float rotation = 0.0f;

	private:
		std::unique_ptr<Engine::Texture> m_texture;
		glm::mat4 m_model = glm::mat4(1.0f);
	};
};

#endif // SPRITE_HPP
