#ifndef UTIL_HPP
#define UTIL_HPP

#include <iostream>
#include <string>

#include <glad/gl.h>

namespace Engine {
	namespace Util {
		[[nodiscard]] bool has_suffix(const std::string &str, const std::string &suffix);

		// read file as string. throws if it failed.
		[[nodiscard]] std::string read_file(const std::string &filename);
	};
};
#ifdef DEBUG
#define GL_CHECK(call) \
	call; \
	{ \
		GLenum err = glGetError(); \
		if (err != GL_NO_ERROR) { \
			std::string err_str; \
			switch (err) { \
				case GL_NO_ERROR:                      err_str = "GL_NO_ERROR"; break; \
				case GL_INVALID_ENUM:                  err_str = "GL_INVALID_ENUM"; break; \
				case GL_INVALID_VALUE:                 err_str = "GL_INVALID_VALUE"; break; \
				case GL_INVALID_OPERATION:             err_str = "GL_INVALID_OPERATION"; break; \
				case GL_STACK_OVERFLOW:                err_str = "GL_STACK_OVERFLOW"; break; \
				case GL_STACK_UNDERFLOW:               err_str = "GL_STACK_UNDERFLOW"; break; \
				case GL_OUT_OF_MEMORY:                 err_str = "GL_OUT_OF_MEMORY"; break; \
				case GL_INVALID_FRAMEBUFFER_OPERATION: err_str = "GL_INVALID_FRAMEBUFFER_OPERATION"; break; \
				default: err_str = std::to_string(err); break; \
			} \
			std::cerr << "(" << __FILE__ << ":" << __LINE__ << ") GL ERROR \"" << err_str << "\"\n"; \
		} \
	}
#else
#define GL_CHECK(call) call;
#endif

#endif // UTIL_HPP
