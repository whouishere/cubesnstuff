#ifndef RENDERER_HPP
#define RENDERER_HPP

#include <iostream>
#include <string>

#include <glad/gl.h>

#include "shader.hpp"
#include "sprite.hpp"

namespace Engine {
	class Renderer {
	public:
		Renderer();
		~Renderer();

		void Clear();
		void Draw();
		void Draw(Engine::Sprite *sprite);

		Shader shader;

	private:
		GLuint m_VAO;
		GLuint m_VBO;
		GLuint m_EBO;

		void DrawElements();
	};
};

#endif // RENDERER_HPP
